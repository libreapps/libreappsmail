/*
# Copyright (C) 2008 Ben Webb <bjwebb@freedomdreams.co.uk>
# This file is part of Libreapps Mail.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
*/

function createac(a, b) {
    // Use an XHRDataSource
    var oDS = new YAHOO.util.XHRDataSource("ajax.php?do=contactac&");
    // Set the responseType
    oDS.responseType = YAHOO.util.XHRDataSource.TYPE_TEXT;
    // Define the schema of the delimited results
    oDS.responseSchema = {
        recordDelim: "\n",
        fieldDelim: "\t",
        fields: ["full","nice"]
    };
    // Enable caching
    oDS.maxCacheEntries = 5;

    // Instantiate the AutoComplete
    var oAC = new YAHOO.widget.AutoComplete(a, b, oDS);
    oAC.queryQuestionMark = false;
    //oAC.useIFrame = true;
    oAC.formatResult = function(oResultData, sQuery, sResultMatch) {
		return oResultData[1];
	};
    
    return {
        oDS: oDS,
        oAC: oAC
    };
};

toAC = createac("to", "tocont");
ccAC = createac("cc", "cccont");
bccAC = createac("bcc", "bcccont");

function messhtml() {	
    //Setup some private variables 
    var Dom = YAHOO.util.Dom, 
    Event = YAHOO.util.Event; 

    //The Editor config 
    var myConfig = { 
        height: '300px', 
        width: '600px', 
        animate: true, 
        dompath: true,
        handleSubmit: true 
    }; 
 
    //Now let's load the Editor.. 
    var myEditor = new YAHOO.widget.Editor('messe', myConfig); 
    myEditor.render(); 
}

