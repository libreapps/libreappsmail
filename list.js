/*
# Copyright (C) 2008 Ben Webb <bjwebb@freedomdreams.co.uk>
# This file is part of Libreapps Mail.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
*/

function hili(num,base) {
	if (document.getElementById("tick"+num).checked == true) {
		document.getElementById("mess"+num).className = base+"_sel";
	}
	else {
		document.getElementById("mess"+num).className = base;
	}
}
function tick(chk,val) {
	if (chk) {
		if (chk.length) {
			for (i = 0; i < chk.length; i++) {
				chk[i].checked = val;
				chk[i].onchange();
			}
		}
		else {
			chk.checked = val;
			chk.onchange();
		}
	}
}
function selall() {
	tick(document.getElementById("listform").check_read,true);
	tick(document.getElementById("listform").check_unread,true);
}
function selnone() {
	tick(document.getElementById("listform").check_read,false);
	tick(document.getElementById("listform").check_unread,false);
}
function selread() {
	tick(document.getElementById("listform").check_read,true);
	tick(document.getElementById("listform").check_unread,false);
}
function selunread() {
	tick(document.getElementById("listform").check_unread,true);
	tick(document.getElementById("listform").check_read,false);
}
function moreact(value) {
	moreacts(value,"");
}
function moreacts(value,tagname) {
	range="";
	first=true;
	for (i in convoarr) {
		if (document.getElementById("tick"+convoarr[i])) {
			if (document.getElementById("tick"+convoarr[i]).checked) {
				if (first) {
					first = false;
				}
				else {
					range += ",";
				}
				range += convoarr[i];
			}
		}
		i++;
	}
	if (range == "") {
		alert("Please select one or more messages.");
	}
	else {
		do_actions(value, tagname, range);
	}
}

