<?php
/*
# Copyright (C) 2008 Ben Webb <bjwebb@freedomdreams.co.uk>
# This file is part of Libreapps Mail.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
*/
include "config.php";
$appname = "Mail";
$pages = array("ajax.php", "default.css", "default_ie.css", "functions.php", "index.php", "install.php", "LICENSE", "list.js", "main.js", "mess.js", "source.php", "star_fill.png", "star_nofill.png", "update.php");

if ($_GET['page'] == "") {
if ($libreapps) { include "../header.php"; ?>
<div id="la-content"><?php } ?>
<h2>Source Code</h2>
<ul>
<?php
	foreach ($pages as $page) {
		if (substr($page, -4) == ".php") {
			echo "<li><a href=\"source.php?page=$page\">$page</a></li>";
		} else {
			echo "<li><a href=\"$page\">$page</a></li>";
		}	
	}
	echo "</ul>";
	if ($libreapps) { echo "</div>";
	include "../footer.php"; }
}
elseif (in_array($_GET['page'], $pages)) {
	header('Content-type: text/plain');
	$file = file($_GET['page']);
	foreach ($file as $line)
		echo $line;
	die();
}
else {
	echo "Access denied.";
}
?>
