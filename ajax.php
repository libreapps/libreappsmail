<?php
/*
# Copyright (C) 2008 Ben Webb <bjwebb@freedomdreams.co.uk>
# This file is part of Libreapps Mail.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
*/

include "config.php";
include "functions.php";
#$mbox = @imap_open("{".$server."/imap/notls}".$folder, $user, $pass);

if ($_GET['do'] == "contactac") {
	$addrs = split("[,;]", $_GET['query']);
	$bit = trim(array_pop($addrs));
	$prev = "";
	foreach ($addrs as $addr) {
		$prev .= trim($addr).", ";
	}
	$comp = "%$bit%";
	if ($result = mysql_query("SELECT * FROM `".$db_prefix."addressbook` WHERE account='$user' AND ( name like '$comp' OR address like '$comp' ) ORDER BY priority DESC, name")); else die(mysql_error());
	#echo $_SERVER['REQUEST_URI']."\t".$_SERVER['REQUEST_URI']."\n";
	while($row=mysql_fetch_array($result)) {
		$name = str_ireplace($bit, "<strong>$bit</strong>", $row['name']);
		$addr = str_ireplace($bit, "<strong>$bit</strong>", $row['address']);
		echo $prev.$row['name']." <".$row['address'].">\t".$name." &lt;".$addr."&gt;"."\n";
	}
}

?>
